# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.5

- patch: Push docker tags

## 0.2.4

- patch: New BB host keys

## 0.2.3

- patch: Fix, target host fingerprint support

## 0.2.2

- patch: Fix, now base64 keys required

## 0.2.1

- patch: Fix dockerhub repo name

## 0.2.0

- minor: Prepare for first deploy.

## 0.1.0

- minor: Initial release
