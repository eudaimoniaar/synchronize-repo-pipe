# IMPORTANT UPDATE
Version 0.2.4
New BB host keys.
More info: https://bitbucket.org/blog/ssh-host-key-changes

# Bitbucket Pipelines Pipe: Synchronize Repo Pipe

The repository in TARGET_REPOSITORY_URL gets completely synchronized with the repository in SOURCE_REPOSITORY_URL whenever a branch was pushend in SOURCE_REPOSITORY_URL. In other words: The copied code in the backup repository is up to date in realtime.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: vidueirof/synchronize-repo-pipe:0.2.5
    variables:
      SOURCE_REPOSITORY_URL: "<string>"
      SOURCE_REPOSITORY_KEY: "<string>"
      TARGET_REPOSITORY_URL: "<string>"
      TARGET_REPOSITORY_KEY: "<string>"
      TARGET_HOST: "<string>" # Optional
      TARGET_FINGERPRINT: "<string>" # Optional
      DEBUG: "<boolean>" # Optional
```
## Variables

| Variable                    | Usage                                                       |
| --------------------------- | ----------------------------------------------------------- |
| SOURCE_REPOSITORY_URL (*)   | The source repository url                                   |
| SOURCE_REPOSITORY_KEY (*)   | The ssh key to read from source repository in base64 format |
| TARGET_REPOSITORY_URL (*)   | The target repository url                                   |
| TARGET_REPOSITORY_KEY (*)   | The name that will be printed in the logs in base64 format  |
| TARGET_HOST                 | If needed, the target host                                  |
| TARGET_FINGERPRINT          | The target host fingerprint to be added as known host       |
| DEBUG                       | Turn on extra debug information. Default: `false`.          |

_(*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: eudaimoniaar/synchronize-repo-pipe:0.2.5
    variables:
      SOURCE_REPOSITORY_URL: "git@bitbucket.org:user/source-repo.git"
      SOURCE_REPOSITORY_KEY: $SECRET_KEY # Set as an secret var in pipeline config
      TARGET_REPOSITORY_URL: "git@bitbucket.org:user/target-repo.git"
      TARGET_REPOSITORY_KEY: $SECRET_KEY
```

Advanced example:

```yaml
script:
  - pipe: eudaimoniaar/synchronize-repo-pipe:0.2.5
    variables:
      SOURCE_REPOSITORY_URL: "git@bitbucket.org:user/source-repo.git"
      SOURCE_REPOSITORY_KEY: $SECRET_KEY # Set as an secret var in pipeline config
      TARGET_REPOSITORY_URL: "git@bitbucket.org:user/target-repo.git"
      TARGET_REPOSITORY_KEY: $SECRET_KEY
      TARGET_HOST: "<string>" # Optional
      TARGET_FINGERPRINT: "<string>" # Optional
```

To get site fingerprint:

```sh
ssh-keyscan -t rsa github.com
```

## Encode/decode base64 keys

LINUX
```sh
base64 -w 0 < my_ssh_key
```

MAC OS X
```sh
base64 < my_ssh_key
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by soporte@eudaimonia.com.ar.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
