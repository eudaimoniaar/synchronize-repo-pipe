#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

SOURCE_REPOSITORY_NAME=repository

# Required parameters
[ -z "$SOURCE_REPOSITORY_URL" ] && fail 'SOURCE_REPOSITORY_URL variable missing.'
[ -z "$SOURCE_REPOSITORY_KEY" ] && fail 'SOURCE_REPOSITORY_KEY variable missing.'
[ -z "$TARGET_REPOSITORY_URL" ] && fail 'TARGET_REPOSITORY_URL variable missing.'
[ -z "$TARGET_REPOSITORY_KEY" ] && fail 'TARGET_REPOSITORY_KEY variable missing.'

# Default parameters
DEBUG=${DEBUG:="false"}

# Optional
if [ ! -z "$TARGET_HOST" ]; then
  info "Adding known host"
  [ -z "$TARGET_FINGERPRINT" ] && fail 'TARGET_FINGERPRINT variable missing.'
  echo "${TARGET_HOST} ssh-rsa ${TARGET_FINGERPRINT}" >> /root/.ssh/known_hosts
fi

eval "$(ssh-agent)"

info "Source repository set to $SOURCE_REPOSITORY_URL"
SOURCE_KEY_DECODED=`echo ${SOURCE_REPOSITORY_KEY} | base64 -d`
ssh-add - <<< "${SOURCE_KEY_DECODED}"

info "Target repository set to $TARGET_REPOSITORY_URL"
TARGET_KEY_DECODED=`echo ${TARGET_REPOSITORY_KEY} | base64 -d`
ssh-add - <<< "${TARGET_KEY_DECODED}"

info "Checking out the source repository"
git clone --bare $SOURCE_REPOSITORY_URL $SOURCE_REPOSITORY_NAME

if [ "$?" != "0" ]; then 
  fail "Failed to clone source"
fi

info "Entering the checked out repository" 
cd $SOURCE_REPOSITORY_NAME

info "Downloading the source repository" 
git fetch origin --tags 

if [ "$?" != "0" ]; then 
  fail "Failed to fetch origin"
fi

# When a fetch mirror is created with --mirror=fetch, the refs will not be 
# stored in the refs/remotes/ namespace, but rather everything in refs/ on
# the remote will be directly mirrored into refs/ in the local repository.
# This option only makes sense in bare repositories, because a fetch would
# overwrite any local commits.
info "Adding the target as mirror remote"
git remote add --mirror=fetch target $TARGET_REPOSITORY_URL

if [ "$?" != "0" ]; then 
  fail "Failed to add target as remote: $TARGET_REPOSITORY_URL"
fi

if [ "$?" != "0" ]; then
  fail "Failed to fetch target"
fi

info "Copying all data from the source to the target repository" 
git push target --all
git push target --tags

if [ "$?" != "0" ]; then
  fail "Failed to push to target"
fi

success "Success!"
