#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="eudaimoniaar/synchronize-repo-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
    #run docker run \
    #    -e NAME="baz" \
    #    -v $(pwd):$(pwd) \
    #    -w $(pwd) \
    #    ${DOCKER_IMAGE}:test

    status=0
    output="Dummy test ok"
    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

